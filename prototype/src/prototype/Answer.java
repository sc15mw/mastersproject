package prototype;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Answer
{
    private int id;
    private String serverFileLocation;
    private String questionNo;

    private List<Overlay> overlays;

    public Answer(int givenId, String givenFileLoc, String givenQuestionNo)
    {
        id = givenId;
        serverFileLocation = givenFileLoc;
        questionNo = givenQuestionNo;

        overlays = new ArrayList<>();
    }

    public Answer(int givenId, String givenFileLoc)
    {
        this(givenId, givenFileLoc, "NONE");
    }

    public static int generateAnswerID()
    {
        //not secure, and may cause collision! change later!
        return ThreadLocalRandom.current().nextInt(1, 100000 + 1);
    }

    //getters and setters
    public void setId(int givenID)
    {
        id = givenID;
    }

    public int getId()
    {
        return id;
    }

    public void setQuestionNo(String givenQuestionNo)
    {
        questionNo = givenQuestionNo;
    }

    public String getQuestionNo()
    {
        return questionNo;
    }

    public void setServerFileLocation(String givenLoc)
    {
        serverFileLocation = givenLoc;
    }

    public String getServerFileLocation()
    {
        return serverFileLocation;
    }

    public void addOverlay(Overlay toAdd)
    {
        //the list can be null if the object has been created using gson (constructor not called)
        //so we need to check
        if (overlays == null)
        {
            overlays = new ArrayList<>();
        }

        overlays.add(toAdd);
    }

    public void addOverlays(List<Overlay> toAdd)
    {
        //add each overlay
        for (int i = 0; i < toAdd.size(); i++)
        {
            addOverlay(toAdd.get(i));
        }
    }

    public Overlay getOverlay(int index)
    {
        return overlays.get(index);
    }

    public List<Overlay> getAllOverlays()
    {
        return overlays;
    }

    //returns the overlay with a matching id. returns null if none found.
    public Overlay getOverlayById(int id)
    {
        Overlay result = null;
        for (int i = 0; i < getNoOfOverlays(); i++)
        {
            if (getOverlay(i).getId() == id)
            {
                result = getOverlay(i);
                break;
            }
        }

        return result;
    }

    public int getNoOfOverlays()
    {
        if (overlays == null)
        {
            return 0;
        }
        else
        {
            return overlays.size();
        }

    }

    @Override
    public String toString()
    {
        return "ID: " + getId() + " Location: " + getServerFileLocation() + " Question No.: "  + getQuestionNo();
    }
}
