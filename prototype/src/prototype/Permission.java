package prototype;

public class Permission
{
    private int id;
    private String username; //the username the permission refers to
    private ScriptPermission permissionLevel;

    public Permission(int givenID, String givenUsername, ScriptPermission givenPermission)
    {
        id = givenID;
        username = givenUsername;
        permissionLevel = givenPermission;
    }

    public Permission(int givenId, String givenUsername, int permissionCode)
    {
        this(givenId, givenUsername, ScriptPermission.fromCode(permissionCode));
    }

    public void setId(int givenId)
    {
        id = givenId;
    }

    public int getId()
    {
        return id;
    }

    public String getIdString()
    {
        return Integer.toString(id);
    }

    public void setUsername(String givenUsername)
    {
        username = givenUsername;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPermissionLevelMessage()
    {
        return permissionLevel.toString();
    }

    public int getPermissionCode()
    {
        return ScriptPermission.toCode(permissionLevel);
    }

    public String getPermissionCodeString()
    {
        return Integer.toString(getPermissionCode());
    }

    public void setPermissionCode(int newCode)
    {
        permissionLevel = ScriptPermission.fromCode(newCode);
    }

    //checks if the given code is a valid permission code
    public static boolean isValidCode(int givenCode)
    {
        return (givenCode == 0 || givenCode == 1 || givenCode == 2);
    }
}
