package prototype;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class PasswordHasher
{
    private String hashAlgorithm;
    private MessageDigest md;

    public PasswordHasher(String givenAlgorithm) throws NoSuchAlgorithmException
    {
        hashAlgorithm = givenAlgorithm;
        md = MessageDigest.getInstance(hashAlgorithm);
    }

    public String hashPassword(String plaintext)
    {
        //get the bytes of the string and hash them
        byte[] hash = md.digest(plaintext.getBytes());

        //encode the hashed bytes as base64 string
        String base64string = Base64.getEncoder().encodeToString(hash);

        System.out.println(base64string);

        return base64string;
    }
}
