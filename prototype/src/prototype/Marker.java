package prototype;

public class Marker
{
    private String username;
    private String passwordHash;

    public Marker(String givenUsername, String givenPassHash)
    {
        username = givenUsername;
        passwordHash = givenPassHash;
    }

    public Marker(String givenUsername)
    {
        this(givenUsername, "NONE");
    }

    public void setUsername(String givenUsername)
    {
        username = givenUsername;
    }

    public String getUsername()
    {
        return username;
    }

    public void setPasswordHash(String givenPassHash)
    {
        passwordHash = givenPassHash;
    }

    public String getPasswordHash()
    {
        return passwordHash;
    }
}
