package prototype.dbmanager;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main extends Application
{
    private String dbFileLocation = "/home/matt/uni/postgrad/thesis/prototypes/server/scripts.db";

    @Override
    public void start(Stage primaryStage)
    {
        //open the database
        try
        {
            String url = "jdbc:sqlite:" + dbFileLocation;
            // create a connection to the database
            Connection conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            MainMenu mm = new MainMenu(primaryStage, conn);
            mm.show();
        }
        catch (SQLException e)
        {
            Alert a = new Alert(Alert.AlertType.ERROR, "Could not open script database.");
            a.showAndWait();
        }





    }
    public static void main(String[] args)
    {
        launch(args);
    }
}
