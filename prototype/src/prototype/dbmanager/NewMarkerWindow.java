package prototype.dbmanager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import prototype.PasswordHasher;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class NewMarkerWindow
{
    private Stage primaryStage;
    private VBox root;
    private double width, height;
    static final double preferredWidth = 300;
    static final double preferredHeight = 400;

    private Connection db;

    private TextField usernameBox, passwordBox;

    private final static String hashingAlgorithm = "SHA-256"; //the hashing algorithm used by the whole system

    public NewMarkerWindow(Stage givenStage, double givenWidth, double givenHeight, Connection givenDatabase)
    {
        primaryStage = givenStage;
        width = givenWidth;
        height = givenHeight;
        db = givenDatabase;

        Label hello = new Label("Add new marker");

        //username section
        HBox usernameSection = new HBox();
        Label userLbl = new Label("Username: ");
        usernameBox = new TextField();
        usernameSection.getChildren().addAll(userLbl, usernameBox);

        //password section
        HBox passwordSection = new HBox();
        Label passwordLbl = new Label("Password");
        passwordBox = new TextField();
        passwordSection.getChildren().addAll(passwordLbl, passwordBox);

        Button addBtn = new Button("Add");
        addBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                try
                {
                    addUser();
                }
                catch (NoSuchAlgorithmException e)
                {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "The selected password hashing algorithm: "   + hashingAlgorithm + "was not found on this system.");
                    alert.showAndWait();
                    System.exit(1);
                }
                catch (SQLException e)
                {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Error adding user");
                    alert.showAndWait();
                }
            }
        });

        root = new VBox();
        root.getChildren().addAll(hello, usernameSection, passwordSection, addBtn);

    }

    //if no height and width is specified, use the recommended ones
    public NewMarkerWindow(Stage givenStage, Connection givenDb)
    {
        this(givenStage, preferredWidth, preferredHeight, givenDb);
    }

    private void addUser() throws NoSuchAlgorithmException, SQLException
    {
        //get the entered username and password
        String username = usernameBox.getText();
        String password = passwordBox.getText();

        //hash the password
        PasswordHasher hasher = new PasswordHasher(hashingAlgorithm);
        String passwordHash = hasher.hashPassword(password);

        //add the marker to the database
        String queryTemplate = "INSERT INTO markers (username, passwordHash) VALUES (?, ?);";
        PreparedStatement query = db.prepareStatement(queryTemplate);
        query.setString(1, username);
        query.setString(2, passwordHash);

        query.executeUpdate();
        query.close();

        //now user has been added, close this window and reopen marker window
        MarkerManager mm = new MarkerManager(primaryStage, db);
        mm.getMarkerInfo();
        hide();
        mm.show();




    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public  void hide()
    {
        primaryStage.hide();
    }
}
