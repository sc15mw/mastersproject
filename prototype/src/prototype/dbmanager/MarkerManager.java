package prototype.dbmanager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import prototype.Marker;
import prototype.Script;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MarkerManager
{
    public Stage primaryStage;

    VBox root;
    TableView markerSelector;
    double width, height;

    List<Marker> markers; //an array storing the markers
    Connection db; //the connection to the script database



    public MarkerManager(Stage givenStage, Connection givenDatabase)
    {

        primaryStage = givenStage;
        db = givenDatabase;

        Button backBtn = new Button("Back");
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {

                MainMenu mm = new MainMenu(primaryStage, db);
                hide();
                mm.show();
            }
        });

        Label hello = new Label("Marker Chooser");

        markerSelector = new TableView();

        TableColumn<Script, String> usernameCol = new TableColumn<>("Username");
        usernameCol.setCellValueFactory(new PropertyValueFactory<>("Username"));


        markerSelector.getColumns().add(usernameCol);


        Button addNewButton = new Button("Add new marker");
        addNewButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {
                NewMarkerWindow nmw = new NewMarkerWindow(primaryStage, db);
                hide();
                nmw.show();

            }
        });
        root = new VBox();

        root.getChildren().addAll(backBtn, hello, markerSelector, addNewButton);

    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public void hide()
    {
        primaryStage.hide();
    }


    //retrieves the list of markers from the database
    public void getMarkerInfo() throws SQLException
    {
        markers = new ArrayList<>();



        Statement stmt = db.createStatement();

        //select all markers
        ResultSet results = stmt.executeQuery("SELECT * FROM markers;");

        //while there is a next result
        while (results.next())
        {
            //get the username
            String username = results.getString("username");

            //create a new marker object
            Marker newMarker = new Marker(username);

            markers.add(newMarker);
            markerSelector.getItems().add(newMarker);

        }

        results.close();
        stmt.close();

    }
}
