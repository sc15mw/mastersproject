package prototype.dbmanager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import prototype.Answer;
import prototype.Permission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//displays info for a particular script
public class ViewScriptWindow
{
    //ui elements
    Stage primaryStage;

    double width, height;
    static final double preferredWidth = 550;
    static final double preferredHeight = 400;

    VBox root;

    TextField studentNameBox;
    TextField scriptIDbox;
    TableView answerView; //a text field displaying all the answers added to the new script
    TableView permissionsView; //displays who has what permissions for this script (contents of "permissions" list)

    Label status;

    //data elements
    Connection db;
    int scriptID; //the id of the script we're displaying
    List<Answer> answers; //stores the answers for this script
    List<Permission> permissions; //stores the permissions for this script

    public ViewScriptWindow(Stage givenStage, double givenWidth, double givenHeight, Connection givenDatabase, int givenID)
    {
        primaryStage = givenStage;
        width = givenWidth;
        height = givenHeight;
        db = givenDatabase;
        scriptID = givenID;

        Button backBtn = new Button("Back");
        backBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                try
                {
                    ScriptManager sm = new ScriptManager(primaryStage, db);
                    sm.getScriptInfo();
                    hide();
                    sm.show();
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                }

            }
        });

        Label hello = new Label("View script");

        //script id section

        Label scriptIDlabel = new Label("Script ID:");

        scriptIDbox = new TextField();
        scriptIDbox.setDisable(true);

        HBox scriptIDSection = new HBox();
        scriptIDSection.getChildren().addAll(scriptIDlabel, scriptIDbox);

        //students name section

        studentNameBox = new TextField();
        HBox studentNameSection = new HBox();

        studentNameSection.getChildren().addAll(new Label("Student name:"), studentNameBox);

        Label permissionsLbl = new Label("Permissions for this script:");

        permissionsView = new TableView();
        permissionsView.setEditable(true);

        TableColumn<Permission, String> idCol = new TableColumn<>("Id");
        idCol.setCellValueFactory(new PropertyValueFactory<>("IdString"));

        TableColumn<Permission, String> usernameCol = new TableColumn<>("Marker Username");
        usernameCol.setCellValueFactory(new PropertyValueFactory<>("Username"));

        TableColumn<Permission, String> codeCol = new TableColumn<>("Permission code");
        codeCol.setCellValueFactory(new PropertyValueFactory<>("PermissionCodeString"));

        codeCol.setCellFactory(TextFieldTableCell.forTableColumn());
        codeCol.setOnEditCommit(event -> {

            //get the permission object that was changed
            Permission editedPermission = event.getRowValue();

            //change the object to reflect the new value
            try
            {
                int newCode = Integer.parseInt(event.getNewValue());
                //check if the code is valid
                if (Permission.isValidCode(newCode))
                {
                    editedPermission.setPermissionCode(newCode);
                }

            }
            catch (NumberFormatException ex)
            {
                Alert alert = new Alert(Alert.AlertType.WARNING, "Permissions can only be 0, 1 or 2.");
                alert.showAndWait();
            }


            //refresh the table
            permissionsView.refresh();
        });

        TableColumn<Permission, String> perCol = new TableColumn<>("Permission Level");
        perCol.setCellValueFactory(new PropertyValueFactory<>("PermissionLevelMessage"));
        permissionsView.getColumns().addAll(idCol, usernameCol, codeCol, perCol);

        status = new Label();

        Button saveBtn = new Button("Save changes");
        saveBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                try
                {
                    //save new permissions and scripts
                    updatePermissions();
                    updateScript();

                    //display status to user
                    status.setText("Changes saved.");

                }
                catch (SQLException e)
                {
                    e.printStackTrace();
                }

            }
        });

        HBox saveSection = new HBox();

        saveSection.getChildren().addAll(saveBtn, status);

        root = new VBox();
        root.getChildren().addAll(backBtn, hello, scriptIDSection, studentNameSection, permissionsLbl, permissionsView, saveSection);

        try
        {
            getScriptInfo(scriptID);
            getPermissions(scriptID);
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }
    }

    public ViewScriptWindow(Stage givenStage, Connection givenDatabase, int givenID)
    {
        this(givenStage, preferredWidth, preferredHeight, givenDatabase, givenID);
    }

    //gets the info the particular script being displayed
    private void getScriptInfo(int scriptID) throws SQLException
    {
        String queryTemplate = "SELECT * FROM scripts WHERE id = ?;";
        PreparedStatement query = db.prepareStatement(queryTemplate);
        query.setString(1, Integer.toString(scriptID));

        ResultSet results = query.executeQuery();

        //we should only have one results, since id is the primary key
        results.next();
        String studentName = results.getString("studentName");

        //set the textbox to the display the value
        scriptIDbox.setText(Integer.toString(scriptID));
        studentNameBox.setText(studentName);


    }

    //gets all the permissionsView for this script
    private void getPermissions(int scriptID) throws SQLException
    {

        permissions = new ArrayList<>();

        //select all the permissions for this script
        String queryTemplate = "SELECT * FROM scriptPermissions WHERE scriptID = ?;";
        PreparedStatement query = db.prepareStatement(queryTemplate);
        query.setString(1, Integer.toString(scriptID));

        ResultSet results = query.executeQuery();

        //we now have all the permissionsView for this script
        while (results.next())
        {
            int id = results.getInt("permissionID");
            String username = results.getString("username");
            int permissionCode = results.getInt("permissionLevel");

            //create an object
            Permission newPermission = new Permission(id, username, permissionCode);
            permissions.add(newPermission);
            permissionsView.getItems().add(newPermission);

        }

    }

    private void updatePermissions() throws SQLException
    {
        String queryTemplate = "UPDATE scriptPermissions SET permissionLevel = ? WHERE permissionID = ?;";

        //for each permission, run an update
        for (int i = 0; i < permissions.size(); i++)
        {
            Permission curPermission = permissions.get(i);

            //only the code is able to be edited so we only need to update the code
            PreparedStatement query = db.prepareStatement(queryTemplate);
            query.setString(1, curPermission.getPermissionCodeString());
            query.setString(2, curPermission.getIdString());

            query.executeUpdate();
            query.close();

        }
    }

    private void updateScript() throws SQLException
    {
        String queryTemplate = "UPDATE scripts SET studentName = ? WHERE id = ?;";

        //only the student name can (currently) be changed
        //get the new student name
        String newName = studentNameBox.getText();

        PreparedStatement query = db.prepareStatement(queryTemplate);
        query.setString(1, newName);
        query.setString(2, Integer.toString(scriptID));

        query.executeUpdate();
        query.close();
    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public  void hide()
    {
        primaryStage.hide();
    }
}
