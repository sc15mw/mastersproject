package prototype.dbmanager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import prototype.Script;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ScriptManager
{
    public Stage primaryStage;

    VBox root;
    TableView scriptSelector;
    double width, height;

    List<Script> scripts; //an array storing the scripts
    Connection db; //the connection to the script database



    public ScriptManager(Stage givenStage, Connection givenDatabase)
    {

        primaryStage = givenStage;
        db = givenDatabase;

        Button backBtn = new Button("Back");
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {

                MainMenu mm = new MainMenu(primaryStage, db);
                hide();
                mm.show();
            }
        });

        Label hello = new Label("Script Chooser");

        scriptSelector = new TableView();

        TableColumn<Script, String> idCol = new TableColumn<>("Script ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("Id"));


        TableColumn<String, Script> perCol = new TableColumn<>("Student Name");
        perCol.setCellValueFactory(new PropertyValueFactory<>("studentName"));

        scriptSelector.getColumns().addAll(idCol, perCol);


        Button addNewButton = new Button("Add new script");
        addNewButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {
                NewScriptWindow nsw = new NewScriptWindow(primaryStage, db);
                nsw.show();
            }
        });

        Button openBtn = new Button("Open/edit");
        openBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                //get the selected script
                Script selectedScript = (Script) scriptSelector.getSelectionModel().getSelectedItem();
                //open it in the script view window
                if (selectedScript != null)
                {
                    ViewScriptWindow vsw = new ViewScriptWindow(primaryStage, db, selectedScript.getId());
                    //hide();
                    vsw.show();
                }

            }
        });

        HBox buttonSection = new HBox();
        buttonSection.getChildren().addAll(addNewButton, openBtn);

        root = new VBox();

        root.getChildren().addAll(backBtn, hello, scriptSelector, buttonSection);

    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public void hide()
    {
        primaryStage.hide();
    }


    //retrieves the list of scripts ids from the database
    public void getScriptInfo() throws SQLException
    {
        scripts = new ArrayList<>();



        Statement stmt = db.createStatement();

        //select all scripts
        ResultSet results = stmt.executeQuery("SELECT * FROM scripts;");

        //while there is a next result
        while (results.next())
        {
            //get the script id and student name
            int id = results.getInt("id");
            String studentName = results.getString("studentName");

            //create a new script, add to the array, and add to the screen
            Script newScript = new Script(id, studentName);
            scripts.add(newScript);
            scriptSelector.getItems().add(newScript);

        }

        results.close();
        stmt.close();

    }
}

