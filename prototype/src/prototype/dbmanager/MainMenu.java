package prototype.dbmanager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.SQLException;

public class MainMenu
{
    public Stage primaryStage;
    double width, height;

    static final double preferredWidth = 200;
    static final double preferredHeight = 300;

    VBox root;

    public MainMenu(Stage givenStage, double givenWidth, double givenHeight, Connection database)
    {
        primaryStage = givenStage;
        width = givenWidth;
        height = givenHeight;

        Label hello = new Label("Main Menu");

        Button scriptsButton = new Button("Manage scripts");
        scriptsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {
                ScriptManager sm = new ScriptManager(primaryStage, database);
                try
                {
                    sm.getScriptInfo();
                    hide();
                    sm.show();

                }
                catch (SQLException ex)
                {

                }
            }
        });

        Button usersButton = new Button("Manage markers");
        usersButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {
                MarkerManager mm = new MarkerManager(primaryStage, database);
                try
                {
                    mm.getMarkerInfo();
                    hide();
                    mm.show();
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                }

            }
        });


        root = new VBox();

        root.getChildren().addAll(hello, scriptsButton, usersButton);
    }

    //if no height or width is given
    public MainMenu(Stage givenStage, Connection database)
    {
        //use the recommended height and width
        this(givenStage, preferredWidth, preferredHeight, database);
    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public void hide()
    {
        primaryStage.hide();
    }
}
