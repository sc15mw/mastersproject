package prototype.dbmanager;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import prototype.Answer;
import prototype.Marker;
import prototype.Script;
import prototype.ScriptPermission;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class NewScriptWindow
{
    //ui elements
    Stage primaryStage;

    double width, height;
    static final double preferredWidth = 300;
    static final double preferredHeight = 400;

    VBox root;

    TextField studentNameBox;
    TableView answerView; //a text field displaying all the answers added to the new script

    //data elements
    Connection db;
    List<Answer> answers; //stores the filepaths of the answers added
    int scriptID; //a unique id for the new script being added

    public NewScriptWindow(Stage givenStage, double givenWidth, double givenHeight, Connection givenDatabase)
    {
        primaryStage = givenStage;
        width = givenWidth;
        height = givenHeight;
        db = givenDatabase;

        //generate a new id
        scriptID = Script.generateScriptID();

        Label hello = new Label("Add new script");

        //script id section

        Label scriptIDlabel = new Label("Script ID (randomly generated):");
        TextField scriptIDbox = new TextField();
        scriptIDbox.setEditable(false);
        scriptIDbox.setText(Integer.toString(scriptID));

        HBox scriptIDSection = new HBox();
        scriptIDSection.getChildren().addAll(scriptIDlabel, scriptIDbox);

        //students name section

        studentNameBox = new TextField();
        HBox studentName = new HBox();
        studentName.getChildren().addAll(new Label("Student name:"), studentNameBox);


        //answers section

        Button addAnswerBtn = new Button("Add answer");
        addAnswerBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {
                try
                {
                    addNewAnswer();
                }
                catch (IOException ex)
                {
                    Alert a = new Alert(Alert.AlertType.ERROR, "Could not open answer.");
                    a.showAndWait();
                }

            }
        });

        HBox answersHeader = new HBox();
        answersHeader.getChildren().addAll(new Label("Answers in this script"), addAnswerBtn);

        //a text field displaying all the answers added so far

        answerView = new TableView();
        answerView.setEditable(true);

        TableColumn<Answer, String> idCol = new TableColumn<>("Answer ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));


        TableColumn<Answer, String> questionCol = new TableColumn<>("Question Number");
        questionCol.setCellFactory(TextFieldTableCell.forTableColumn());
        questionCol.setCellValueFactory(new PropertyValueFactory<>("questionNo"));

        //this allows the question number to be updated by the user. No other fields need to be edited
        questionCol.setOnEditCommit(event -> {
            //get the answer that has been modified and set its new question number
            event.getRowValue().setQuestionNo(event.getNewValue());
        });

        TableColumn<Answer, String> filepathCol = new TableColumn<>("Filepath");
        filepathCol.setCellValueFactory(new PropertyValueFactory<>("serverFileLocation"));

        answerView.getColumns().addAll(idCol, questionCol, filepathCol);

        answers = new ArrayList<>();


        Button addBtn = new Button("Add script");
        addBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e)
            {
                try
                {
                    addScript();
                }
                catch (SQLException ex)
                {
                    ex.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Could not add script");
                    alert.showAndWait();
                }

            }
        });

        root = new VBox();
        root.getChildren().addAll(hello,scriptIDSection, studentName, answersHeader, answerView, addBtn);


    }

    //if no height or width is given, use recommended height and width
    public NewScriptWindow(Stage givenStage, Connection givenDatabase)
    {
        this(givenStage, preferredWidth, preferredHeight, givenDatabase);
    }

    private void addNewAnswer() throws IOException
    {
        //open filechooser and have user select the file
        FileChooser fileChooser = new FileChooser();
        File answerFile = fileChooser.showOpenDialog(primaryStage);

        //get the file path
        String filepath = answerFile.getCanonicalPath();

        //generate an id for the new answer
        int newID = Answer.generateAnswerID();

        //create an object
        Answer newAnswer = new Answer(newID, filepath);

        //add to lists
        answers.add(newAnswer);
        answerView.getItems().add(newAnswer);


    }

    private void addScript() throws SQLException
    {
        //get all the info from the text fields
        String studentName = studentNameBox.getText();

        //firstly add the script itself (id and student name)
        String queryTemplate = "INSERT INTO scripts (id, studentName) VALUES (?, ?);";
        PreparedStatement query = db.prepareStatement(queryTemplate);
        query.setString(1, Integer.toString(scriptID));
        query.setString(2, studentName);

        query.executeUpdate();
        query.close();


        //secondly add all the answers to the "answers" table
        for (int i = 0; i < answers.size(); i++)
        {
            Answer curAnswer = answers.get(i);
            System.out.println(curAnswer.toString());

            queryTemplate = "INSERT INTO answers (id, scriptID, questionNo, serverFileLocation) VALUES (?, ?, ?, ?);";
            query = db.prepareStatement(queryTemplate);

            query.setString(1, Integer.toString(curAnswer.getId()));
            query.setString(2, Integer.toString(scriptID));
            query.setString(3, curAnswer.getQuestionNo());
            query.setString(4, curAnswer.getServerFileLocation());

            query.executeUpdate();
            query.close();

        }

        //finally, set the permissionsView for this new script

        //the default permission is none. get the code for this.
        int defaultCode = ScriptPermission.toCode(ScriptPermission.NO_PERMISSIONS);

        //set up a query template
        queryTemplate = "INSERT INTO scriptPermissions (permissionID, scriptID, username, permissionLevel) VALUES (?, ?, ?, ?);";


        //pull up all the users
        Statement stmt = db.createStatement();
        ResultSet results = stmt.executeQuery("SELECT * FROM markers;");

        //while there is a next result
        while (results.next())
        {
            //get the username
            String username = results.getString("username");

            //generate an id
            int permissionId = ScriptPermission.generatePermissionID();

            //give this user "NO_PERMISSIONS" by default
            query = db.prepareStatement(queryTemplate);
            query.setString(1, Integer.toString(permissionId));
            query.setString(2, Integer.toString(scriptID));
            query.setString(3, username);
            query.setString(4, Integer.toString(defaultCode));

            query.executeUpdate();
            query.close();

            System.out.println("Added permission: " + defaultCode + " for user " + username + " on script id: " + scriptID);


        }

        results.close();
        stmt.close();

    }



    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    public  void hide()
    {
        primaryStage.hide();
    }
}
