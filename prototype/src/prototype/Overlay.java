package prototype;

import java.util.concurrent.ThreadLocalRandom;

public class Overlay
{
    private int id;
    private int answerID;
    private String serverFileLocation;

    public Overlay(int givenid, int givenanswerId, String givenFileLoc)
    {
        id = givenid;
        answerID = givenanswerId;
        serverFileLocation = givenFileLoc;
    }

    public Overlay(int givenid, int givenanswerId)
    {
        this(givenid, givenanswerId, null);
    }

    public static int generateOverlayID()
    {
        //not secure, and may cause collision! change later!
        return ThreadLocalRandom.current().nextInt(1, 100000 + 1);
    }

    //getters
    public int getId()
    {
        return id;
    }

    public int getAnswerID()
    {
        return answerID;
    }

    public String getServerFileLocation()
    {
        return serverFileLocation;
    }

    //setters
    void setId(int givenID)
    {
        id = givenID;
    }

    void setAnswerID(int givenAnswerID)
    {
        answerID = givenAnswerID;
    }

    void setServerFileLocation(String givenLocation)
    {
        serverFileLocation = givenLocation;
    }

    @Override
    public String toString()
    {
        return "ID: " + getId() + ". Answer: " + getAnswerID() + ". File located at: " + getServerFileLocation();
    }
}
