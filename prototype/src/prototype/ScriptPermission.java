package prototype;

import java.util.concurrent.ThreadLocalRandom;

public enum ScriptPermission
{
    NO_PERMISSIONS (0, "No permissions to view or edit"),
    READ_ONLY(1, "Permission to read script"),
    READ_WRITE(2, "Permission to read and edit script");

    private int numericCode;
    private String msg;

    ScriptPermission(int givenCode, String givenMsg)
    {
        numericCode = givenCode;
        msg = givenMsg;
    }

    //converts the object to and from a numeric code (this is how it is stored in the database).
    static public ScriptPermission fromCode(int permissionCode)
    {
        switch (permissionCode)
        {
            case 0:
                return ScriptPermission.NO_PERMISSIONS;
            case 1:
                return ScriptPermission.READ_ONLY;
            case 2:
                return ScriptPermission.READ_WRITE;
        }

        return ScriptPermission.NO_PERMISSIONS;
    }

    static public int toCode(ScriptPermission permission)
    {
        return permission.numericCode;
    }

    @Override
    public String toString()
    {
        return msg;
    }

    public static int generatePermissionID()
    {
        //not secure, and may cause collision! change later!
        return ThreadLocalRandom.current().nextInt(1, 100000 + 1);
    }
}
