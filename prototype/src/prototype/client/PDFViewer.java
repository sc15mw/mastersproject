package prototype.client;

import javafx.embed.swing.SwingFXUtils;
import javafx.embed.swt.SWTFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import prototype.Answer;
import prototype.Overlay;
import prototype.Script;

import javafx.scene.paint.Color;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class PDFViewer
{
    private Stage primaryStage = null; //the given stage displaying the viewer

    private Script displayedScript; //the script currently being displayed
    private int curQuestionIndex; //the question number

    private Overlay displayedOverlay; //the current overlay
    private Image overlayImg;

    private PDDocument displayedDoc = null; //the PDF currently being displayed
    private List<Image> pages = null; //the pages of the pdf as images, ready to be displayed

    private String serverAddress;
    private String token;

    private VBox root;

    private ImageView answerViewer; //displays the answer image
    private ImageView overlayViewer; //displays the overlay image

    private BorderPane imagePane;
    private double width, height;
    private double maxWidth, maxHeight;

    private int lastClickedX = 0; //where the user last clicked on the overlay
    private int lastClickedY = 0;

    private int lastClickedXimg = 0; //where the user last clicked (in image coordinates)
    private int lastClickedYimg = 0;

    Button prevPageBtn;
    Button nextPageBtn;

    TextField questionBox; //displays the current question no.
    Label totalQuestionsLabel; //displays the total no of questions

    TextField commentBox;
    Button addCommentBtn;

    ComboBox<String> overlaySelect; //ticked if the answer overlay should be displayed

    public PDFViewer(Stage givenStage)
    {
        primaryStage = givenStage;

        primaryStage.setTitle("Script Viewer");

        HBox infoBar = new HBox();

        prevPageBtn = new Button("Prev.");
        prevPageBtn.setDisable(true);
        prevPageBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                try
                {
                    displayPrevAnswer();
                }
                catch (IOException ex)
                {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Could not open previous question");
                }

            }
        });

        nextPageBtn = new Button("Next");
        nextPageBtn.setDisable(true);
        nextPageBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                try
                {
                    displayNextAnswer();
                }
                catch (IOException ex)
                {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Could not open next question");
                    alert.showAndWait();
                }

            }
        });

        Label questionLabel = new Label("Q.:");
        questionBox = new TextField();
        questionBox.setDisable(true);

        Label slash = new Label("/");

        totalQuestionsLabel = new Label();

        Label overlayLabel = new Label("Overlays:");

        overlaySelect = new ComboBox<>();
        overlaySelect.setEditable(true);
        overlaySelect.setOnAction(new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent e) {
            //get the selected item
            String selected = overlaySelect.getSelectionModel().getSelectedItem();
            if (selected.equals("<No overlay>"))
            {
                removeOverlay();
            }
            else
            {
                try
                {
                    //get the answer currently being displayed
                    Answer displayedAnswer = displayedScript.getAnswer(curQuestionIndex);

                    //the selected option is the id of the overlay to apply
                    int selectedId = Integer.parseInt(selected);

                    //find a matching overlay in the displayed answer
                    Overlay selectedOverlay = displayedAnswer.getOverlayById(selectedId);

                    //check if its null
                    if (selectedOverlay != null)
                    {
                        overlayImg = downloadOverlay(selectedOverlay);
                        applyOverlay(selectedOverlay, overlayImg);

                    }
                }
                catch (NumberFormatException ex)
                {
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter a valid overlay ID");
                    alert.showAndWait();
                }



            }
        }
        });

        Label commentLbl = new Label("Add new comment: ");
        commentBox = new TextField();
        commentBox.setDisable(true);

        addCommentBtn = new Button("Add");
        //disable add button until user selects an overlay
        addCommentBtn.setDisable(true);
        addCommentBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                if (displayedOverlay != null)
                {
                    //disable add button while we work
                    addCommentBtn.setDisable(true);
                    String newComment = commentBox.getText();

                    //get the overlay image
                    overlayImg = overlayViewer.getImage();
                    overlayImg = addComment(overlayImg, lastClickedXimg, lastClickedYimg, newComment);

                    //reapply the overlay with new image
                    applyOverlay(displayedOverlay, overlayImg);

                    //clear text box
                    commentBox.clear();
                    //re-enable add button
                    addCommentBtn.setDisable(false);
                }
            }
        });

        Button newOverlayBtn = new Button("New");
        newOverlayBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                //generate an id for the new answer
                int newId = Overlay.generateOverlayID();
                //get the id of the current question
                int answerId = displayedScript.getAnswer(curQuestionIndex).getId();

                Overlay newOverlay = new Overlay(newId, answerId);

                //add this new overlay to the select box
                overlaySelect.getItems().add(Integer.toString(newId));
                overlaySelect.getSelectionModel().selectLast();

                //create a blank overlay with the same size as the answer
                int answerHeight = (int) pages.get(0).getHeight();
                int answerWidth = (int) pages.get(0).getWidth();
                overlayImg = createEmptyOverlay(answerWidth, answerHeight);

                //apply this new blank overlay
                applyOverlay(newOverlay, overlayImg);
            }
        });

        Button saveOverlayBtn = new Button("Save");
        saveOverlayBtn.setOnAction(new EventHandler<ActionEvent>()
        {
            @Override
            public void handle(ActionEvent actionEvent)
            {
                try
                {
                    sendOverlay(displayedOverlay, overlayImg);
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.ERROR, "Could not send overlay to server");
                    alert.showAndWait();
                }

            }
        });

        infoBar.getChildren().addAll(prevPageBtn, nextPageBtn, questionLabel, questionBox,slash, totalQuestionsLabel, overlayLabel, overlaySelect, newOverlayBtn, saveOverlayBtn, commentLbl, commentBox, addCommentBtn);

        answerViewer = new ImageView();
        imagePane = new BorderPane();


        //calculate maximum size the window can be (the size of the screen)
        maxWidth = Screen.getPrimary().getVisualBounds().getWidth();
        maxHeight = Screen.getPrimary().getVisualBounds().getHeight();

        //set the height and width of the window to the size of document, unless it exceeds the screen size

        //put viewer at centre of pane
        imagePane.setCenter(answerViewer);

        root = new VBox();
        root.getChildren().addAll(infoBar, imagePane);


    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }

    private Image createEmptyOverlay(int width, int height)
    {
        //create a new writeable image
        WritableImage newOverlay = new WritableImage(width, height);
        PixelWriter pw = newOverlay.getPixelWriter();

        //set all pixels to transparent (there is probably a faster way to do this)
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                pw.setColor(i, j, Color.TRANSPARENT);
            }
        }

        return newOverlay;
    }

    //set the pdf to be displayed
    public void setPDF(String url) throws IOException
    {

        //open document and convert them to images
        displayedDoc = openURL(url);
        pages = convertPDF(displayedDoc);
        displayedDoc.close();

        //display first page
        System.out.println("Width: " + pages.get(0).getWidth() + " Height: " + pages.get(0).getHeight());
        answerViewer.setImage(pages.get(0));

        //resize viewer to always fill window, preserving the aspect ratio
        answerViewer.fitWidthProperty().bind(primaryStage.widthProperty());
        answerViewer.fitHeightProperty().bind(primaryStage.heightProperty());
        answerViewer.setPreserveRatio(true);

        width = Math.min(pages.get(0).getWidth(), maxWidth);
        height = Math.min(pages.get(0).getHeight(), maxHeight);

    }

    //opens the given script
    public void setScript(Script givenScript, String givenServerAddress, String givenToken) throws IOException
    {
        displayedScript = givenScript;
        serverAddress = givenServerAddress;
        token = givenToken;

        //set the total number of answers
        totalQuestionsLabel.setText(Integer.toString(displayedScript.getNoOfAnswers()));

        //if there are more than 1 answers, we need the next button
        if (displayedScript.getNoOfAnswers() > 1)
        {
            nextPageBtn.setDisable(false);
        }

        //display the first answer of the script
        curQuestionIndex = 0;

        //set the current question number (0 + 1 = 1)
        questionBox.setText(Integer.toString(curQuestionIndex + 1));

        setAnswer(curQuestionIndex);

    }

    private void setAnswer(int questionIndex) throws IOException
    {
        Answer toDisplay = displayedScript.getAnswer(questionIndex);
        setPDF(serverAddress + "getanswer?id=" + toDisplay.getId() + "&token=" + token);

        //update the combobox displaying the available overlays
        //clear any existing items
        overlaySelect.getItems().clear();

        //add the option for "No overlay
        overlaySelect.getItems().add("<No overlay>");

        //add the new answers overlays
        //get each overlay, and add its id to the drop down
        for (int i = 0; i < toDisplay.getNoOfOverlays(); i++)
        {
            //convert the overlay's id to a string
            String idString = Integer.toString(toDisplay.getOverlay(i).getId());
            overlaySelect.getItems().add(idString);
        }

        overlaySelect.getSelectionModel().selectFirst();

    }

    //uses a FileChooser to have the user open a valid pdf, and returns it as a PDDocument
    private static PDDocument openFile(Stage stage) throws IOException
    {
        FileChooser openFile = new FileChooser();
        File chosenFile = openFile.showOpenDialog(stage);

        //load the document
        PDDocument document = PDDocument.load(chosenFile);
        return document;
    }

    //returns a pdf document located at the given url
    private static PDDocument openURL(String givenURL) throws IOException
    {
        URL url = new URL(givenURL);
        //File onlineFile = new File(url);

        //load document
        PDDocument document = PDDocument.load(url.openStream());
        return document;

    }

    //converts the given pdf document into a set of Image objects, one for each page
    private static List<Image> convertPDF(PDDocument pdfDoc) throws IOException
    {
        //arrays of images: one for each page
        List<Image> pages = new ArrayList<>();

        PDFRenderer renderer = new PDFRenderer(pdfDoc);

        int noPages = pdfDoc.getNumberOfPages();

        //if there are no pages, the given file was empty
        if (noPages == 0)
        {
            System.out.println(noPages);
            throw new IOException();
        }

        for (int pageNo = 0; pageNo < noPages; pageNo++)
        {
            //render the page as a BufferedImage
            BufferedImage temp = renderer.renderImageWithDPI(pageNo, 300, ImageType.RGB);
            //convert to javafx's Image object
            pages.add(SwingFXUtils.toFXImage(temp, null));
        }



        return pages;
    }

    private void displayNextAnswer() throws IOException
    {
        //increment the current question number
        curQuestionIndex++;

        //display the new question
        setAnswer(curQuestionIndex);

        //enable previous button
        prevPageBtn.setDisable(false);

        //check if we need to disable the next button

        if (curQuestionIndex >= displayedScript.getNoOfAnswers() - 1)
        {
            nextPageBtn.setDisable(true);
        }

        //update the number
        //note: we add 1 to the index when displaying to the user, since non-programmers don't like counting from 0!
        questionBox.setText(Integer.toString(curQuestionIndex + 1));
    }

    private void displayPrevAnswer() throws IOException
    {
        curQuestionIndex--;
        setAnswer(curQuestionIndex);

        //enable next button
        nextPageBtn.setDisable(false);
        //check if we need to disable previous button
        if (curQuestionIndex == 0)
        {
            prevPageBtn.setDisable(true);
        }
        questionBox.setText(Integer.toString(curQuestionIndex + 1));
    }

    //downloads the overlays image from the server
    private Image downloadOverlay(Overlay toDownload)
    {
        //retrieve the overlay as a png
        String url = serverAddress + "getoverlay?id=" + toDownload.getId() + "&token=" + token;
        return new Image(url);
    }

    //sends the given overlay to the server
    private void sendOverlay(Overlay toSend, Image toSendImg) throws IOException
    {
        //write the overlay to a temporary file
        File tempFile = new File("temp.png");
        ImageIO.write(SwingFXUtils.fromFXImage(toSendImg, null), "png", tempFile);

        final String boundary = "*****"; //the string separating parts of the request
        final String crlf = "\r\n";
        final String hyphens = "--";

        URL url = new URL(serverAddress + "addoverlay?id=" + toSend.getId() + "&token=" + token);
        System.out.println(url.toString());

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        //connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

        DataOutputStream out = new DataOutputStream(connection.getOutputStream());

        out.writeBytes(hyphens + boundary + crlf);
        out.writeBytes("Content-Disposition: form-data;id=\"" + toSend.getId() + "\";answerID=\"" + toSend.getAnswerID() +"\"" + crlf);
        out.writeBytes(crlf);

        byte[] imgbytes = Files.readAllBytes(tempFile.toPath());
        out.write(imgbytes);



        //end multi-part data
        out.writeBytes(crlf);
        out.writeBytes(hyphens + boundary + hyphens + crlf);


        out.flush();
        out.close();

        System.out.println("Waiting for response");

        int status = connection.getResponseCode();
        System.out.println(status);

        connection.disconnect();




    }

    //takes the given overlay and image and merges it with the currently displayed answer
    private void applyOverlay(Overlay toApply, Image toApplyImg)
    {
        displayedOverlay = toApply;

        //enable commenting features
        commentBox.setDisable(false);
        addCommentBtn.setDisable(false);

        //create two new image views: one for answer, one for overlay
        overlayViewer = new ImageView();
        overlayViewer.fitWidthProperty().bind(primaryStage.widthProperty());
        overlayViewer.fitHeightProperty().bind(primaryStage.heightProperty());
        overlayViewer.setPreserveRatio(true);
        overlayViewer.setImage(toApplyImg);

        answerViewer = new ImageView();
        answerViewer.fitWidthProperty().bind(primaryStage.widthProperty());
        answerViewer.fitHeightProperty().bind(primaryStage.heightProperty());
        answerViewer.setPreserveRatio(true);
        answerViewer.setImage(pages.get(0));


        //group the existing image on the bottom, with the overlay on top
        overlayViewer.setBlendMode(BlendMode.SRC_OVER);

        Group blended = new Group(answerViewer, overlayViewer);
        blended.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                //update where the user last clicked
                lastClickedX = (int) mouseEvent.getX();
                lastClickedY = (int) mouseEvent.getY();

                //calculate aspect ratio
                double aspectRatio = overlayImg.getWidth()/overlayImg.getHeight();

                //use it to calculate the actual dimensions of the image as it is displayed
                //(bizarrely, there are no inbuilt functions for this)
                double displayedWidth =  Math.min(overlayViewer.getFitWidth(), overlayViewer.getFitHeight()*aspectRatio);
                double displayedHeight =  Math.min(overlayViewer.getFitHeight(), overlayViewer.getFitWidth()/aspectRatio);


                //using the proproption of the actual image and the displayed image
                //we can convert from the displayed image coords to the actual image cooords
                lastClickedXimg = (int) (lastClickedX * (overlayImg.getWidth()/displayedWidth));
                lastClickedYimg = (int) (lastClickedY * (overlayImg.getHeight()/displayedHeight));
            }
        });

        //place the blended image in the image pane instead of imageviewer
        imagePane.setCenter(blended);
    }

    //removes the current overlay (if any), displaying just the answer
    private void removeOverlay()
    {
        displayedOverlay = null;

        //disable commenting features
        commentBox.setDisable(true);
        addCommentBtn.setDisable(true);

        //set the imageviewer to display the first page of the answer
        answerViewer.setImage(pages.get(0));

        //place the imageview back in the image pane
        imagePane.setCenter(answerViewer);
        System.out.println("Overlay removed");
    }

    //adds a text comment to the given image at the given position
    private Image addComment(Image overlay, int x, int y, String comment)
    {
        System.out.println("Adding: " + comment + " at: " + x + " " +  y);
        //convert image into awt bufferedimage
        BufferedImage image = SwingFXUtils.fromFXImage(overlay, null);

        Graphics graphics = image.getGraphics();
        graphics.setFont(new Font("TimesRoman", Font.PLAIN, 100));
        graphics.drawString(comment, x, y);

        graphics.dispose();

        //convert back into image
        return SwingFXUtils.toFXImage(image, null);

    }


}
