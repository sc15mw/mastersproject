package prototype.client;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application
{
    String serverAddress = "http://localhost:8002/";
    @Override
    public void start(Stage primaryStage)
    {

        /*
        ScriptChooser sc = new ScriptChooser(primaryStage, serverAddress);
        try
        {
            sc.getScriptInfo();
            sc.show();
        }
        catch (ConnectException e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Could not connect to server.");
            alert.showAndWait();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


        */
        UserLogin ul = new UserLogin(primaryStage, serverAddress);
        ul.show();


    }
    public static void main(String[] args)
    {
        launch(args);
    }
}
