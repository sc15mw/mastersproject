package prototype.client;

import com.google.gson.Gson;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import prototype.Answer;
import prototype.Overlay;
import prototype.Script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class ScriptChooser
{
   public Stage primaryStage;

   VBox root;
   TableView scriptSelector;
   double width, height;

   String serverAddress;
   List<Script> scripts; //an array storing the scripts

    String username;
    String token;




   public ScriptChooser(Stage givenStage, String givenURL, String givenUsername, String givenToken)
   {
       serverAddress = givenURL;
       primaryStage = givenStage;

       username = givenUsername;
       token = givenToken;

       Label hello = new Label("Choose a script to open");
       primaryStage.setTitle("Script Chooser");
       scriptSelector = new TableView();

       TableColumn<Script, String> idCol = new TableColumn<>("Script ID");
       idCol.setCellValueFactory(new PropertyValueFactory<>("Id"));

       TableColumn<Script, String> studentNameCol = new TableColumn<>("Student");
       studentNameCol.setCellValueFactory(new PropertyValueFactory<>("studentName"));

       TableColumn<String, Script> perCol = new TableColumn<>("Permission Level");
       perCol.setCellValueFactory(new PropertyValueFactory<>("PermissionLevelMessage"));

       scriptSelector.getColumns().addAll(idCol, studentNameCol, perCol);


       Button openButton = new Button("Open");
       openButton.setOnAction(new EventHandler<ActionEvent>() {
           @Override public void handle(ActionEvent e) {
               //get the selected script
               Script selectedScript = (Script) scriptSelector.getSelectionModel().getSelectedItem();

               //open it
               if (selectedScript != null)
               {
                   try
                   {
                       openScript(selectedScript);
                   }
                   catch (IOException ex)
                   {
                       Alert a = new Alert(Alert.AlertType.ERROR, "Could not open script.");
                       ex.printStackTrace();
                       a.showAndWait();
                   }
               }


           }
       });

       root = new VBox();

       root.getChildren().addAll(hello, scriptSelector, openButton);

   }

   public void show()
   {
       primaryStage.setScene(new Scene(root, width, height));
       primaryStage.show();
   }

   //opens the given script in the pdfviewer
   private void openScript(Script scriptToOpen) throws IOException
   {
       //get the details of the answers of this script
       List <Answer> answers = getAnswers(scriptToOpen);

       //for each answer
       for (int i = 0; i < answers.size(); i++)
       {
           Answer currentAnswer = answers.get(i);
           //get the overlays for this answer
           List<Overlay> overlays = getOverlays(currentAnswer);

           //add them to this answer
           currentAnswer.addOverlays(overlays);
       }

       //add the answer to the script
       scriptToOpen.addAnswers(answers);

       //open a pdf viewer and close this window
       PDFViewer viewer = new PDFViewer(primaryStage);

       viewer.setScript(scriptToOpen, serverAddress, token);
       viewer.show();
   }


   //returns a list of answers for the given script
   private List<Answer> getAnswers(Script givenScript) throws IOException
   {
       //get a list of answer ids that make up this script
       URL url = new URL(serverAddress + "getanswerids?id=" + givenScript.getId() + "&token=" + token);
       BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
       String inputLine = in.readLine();
       in.close();

       //convert the string of ids into an array
       String[] ids = splitIds(inputLine);

       //get info for each answer from the server
       Gson mygson = new Gson();

       List<Answer> results = new ArrayList<>();

       for (int i = 0; i < ids.length; i++)
       {
           url = new URL(serverAddress + "getanswerinfo?id=" + ids[i] + "&token=" + token);
           in = new BufferedReader(new InputStreamReader(url.openStream()));
           inputLine = in.readLine();


           //convert json string received from server into Answer object
           Answer newAnswer = mygson.fromJson(inputLine, Answer.class);

           results.add(newAnswer);


       }

       return results;
   }

   //retrieves a list of overlays (if any) for a given answer
   private List<Overlay> getOverlays(Answer givenAnswer) throws IOException
   {
       //for the answer get the list of overlays
       URL url = new URL(serverAddress + "getoverlayids?id=" + givenAnswer.getId() + "&token=" + token);
       BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
       String inputLine = in.readLine();

       System.out.println(inputLine);

       //convert string into an array of ids
       String[] ids = splitIds(inputLine);



       List<Overlay> results = new ArrayList<>();
       Gson mygson = new Gson();

       //if "ids" is null, there are no overlays for this answer, so return an empty list
       if (ids != null)
       {
           //for each id...
           for (int i = 0; i < ids.length; i++)
           {
               url = new URL(serverAddress + "getoverlayinfo?id=" + ids[i] + "&token=" + token);
               in = new BufferedReader(new InputStreamReader(url.openStream()));
               inputLine = in.readLine();


               //convert json string received from server into overlay object
               Overlay newOverlay = mygson.fromJson(inputLine, Overlay.class);

               System.out.println(newOverlay.toString());

               results.add(newOverlay);
           }
       }

       return results;
   }

   //splits the JSON array into a set of strings
   private String[] splitIds(String idsString)
   {
       //if given string is an empty array "[]", return null
       if (idsString.equals("[]"))
       {
           return null;
       }
       else
       {
           //remove first and last character ("[" and "]")
           String temp = idsString.substring(1, idsString.length() - 1);

           //split into individual ids
           String[] ids = temp.split(",");

           return ids;
       }

   }

   //retrieves the list of scripts ids from the server
   public void getScriptInfo() throws IOException
   {
       //get the script ids
       URL url = new URL(serverAddress + "scriptids?token=" + token);
       BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
       String inputLine = in.readLine();
       in.close();

       String ids[] = splitIds(inputLine);

       //create an array to store the script objects
       scripts = new ArrayList<>();

       Gson mygson = new Gson();

       //for each id...
       for (int i = 0; i < ids.length; i++)
       {
           //read in the info for each script
           url = new URL(serverAddress + "getscriptinfouser?scriptid=" + ids[i] + "&username=" + username + "&token=" + token);
           in = new BufferedReader(new InputStreamReader(url.openStream()));
           inputLine = in.readLine();
           in.close();

           System.out.println(inputLine);

           if (inputLine != null)
           {
               //convert to Script object and add to array
               Script temp = mygson.fromJson(inputLine, Script.class);

               //get a list of answer ids that make up this script
               url = new URL(serverAddress + "getanswerids?id=" + ids[i] + "&token=" + token);
               in = new BufferedReader(new InputStreamReader(url.openStream()));
               inputLine = in.readLine();
               in.close();

               scripts.add(temp);
               scriptSelector.getItems().add(temp);
           }


       }

   }
}
