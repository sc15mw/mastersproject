package prototype.client;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import prototype.PasswordHasher;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


public class UserLogin
{
    Stage primaryStage;

    String serverAddress;

    VBox root;

    double width, height;

    TextField usernameBox;
    PasswordField passwordBox;

    Label errorMsg;

    String hashAlgorithm = "SHA-256";

    public UserLogin(Stage givenStage, String givenURL)
    {
        serverAddress = givenURL;
        primaryStage = givenStage;

        primaryStage.setTitle("Login");
        Label hello = new Label("Enter your username and password below");


        HBox usernameSection = new HBox();
        usernameSection.getChildren().add(new Label("Username:"));


        usernameBox = new TextField();
        usernameSection.getChildren().add(usernameBox);

        HBox passwordSection = new HBox();
        passwordSection.getChildren().add(new Label("Password:"));

        passwordBox = new PasswordField();
        passwordSection.getChildren().add(passwordBox);

        errorMsg = new Label("");

        Button loginButton = new Button("Log in");
        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                System.out.println("login");

                try
                {
                    //hash the password the user entered
                    PasswordHasher hasher = new PasswordHasher(hashAlgorithm);
                    String hashedPass = hasher.hashPassword(passwordBox.getText());

                    //get the username
                    String username = usernameBox.getText();

                    //send login details to get a token
                    String token = sendLoginDetails(usernameBox.getText(), hashedPass);

                    System.out.println(token);

                    //open the script menu
                    ScriptChooser sc = new ScriptChooser(primaryStage, serverAddress, username, token );
                    sc.getScriptInfo();
                    primaryStage.hide();
                    sc.show();

                }
                catch (NoSuchAlgorithmException ex)
                {
                    Alert al = new Alert(Alert.AlertType.ERROR,"The selected password hashing algorithm \"" + hashAlgorithm +"\" is unavailable.");
                    al.showAndWait();
                    System.exit(1);
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                    Alert al = new Alert(Alert.AlertType.ERROR, "Could not login.");
                    al.showAndWait();
                    System.exit(1);
                }
                catch (SignInException ex)
                {
                    System.out.println(ex.toString());
                    errorMsg.setText("Incorrect username/password");
                }


            }
        });

        root = new VBox();

        root.getChildren().addAll(hello, usernameSection, passwordSection, errorMsg,loginButton);

    }

    public void show()
    {
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();
    }



    //sends the given login details and returns the token issued by the server
    private String sendLoginDetails(String username, String password) throws IOException, SignInException
    {
        URL url = new URL(serverAddress + "login");
        System.out.println(url.toString());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        //connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        //convert username and password into a parameter string
        String parameters = "username=" + username + "&password=" + password;

        //send
        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        out.writeBytes(parameters);
        out.flush();
        out.close();

        //get response code
        int responseCode = connection.getResponseCode();
        //if the response is 401 (unauthorised), the login details were wrong
        if (responseCode == 401)
        {
            throw new SignInException("Incorrect username/password");
        }


        //read token
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String response = in.readLine();




        return response;



    }

}
