package prototype.client;

public class SignInException extends Exception
{
    String msg;

    public SignInException(String givenMsg)
    {
        msg = givenMsg;
    }

    @Override
    public String toString()
    {
        return msg;
    }
}
