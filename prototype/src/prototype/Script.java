package prototype;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Script
{
    private int id;
    private String studentName;
    private List<Answer> answers; //the individual answers that make up this scripts

    private int permissionLevel; //the level of permission the user has for this script (0 = no permission, 1 = read only, 2 = read/write)

    public Script(int givenId, int givenPermissionCode)
    {
        answers = new ArrayList<>();
        permissionLevel = givenPermissionCode;
        id = givenId;

    }

    public Script(int givenId, String givenStudentName)
    {
        answers = new ArrayList<>();
        id = givenId;
        studentName = givenStudentName;
    }


    public Script(String givenId, String givenFilename, ScriptPermission givenPermissionLevel)
    {
        this(Integer.parseInt(givenId), ScriptPermission.toCode(givenPermissionLevel));
    }



    public int getId()
    {
        return id;
    }


    public ScriptPermission getPermissionLevel()
    {
        return ScriptPermission.fromCode(permissionLevel);
    }

    public String getPermissionLevelMessage()
    {
        return getPermissionLevel().toString();
    }

    public void setId(int givenID)
    {
        id = givenID;
    }

    public void setPermissionLevel(int givenPermissionLevel)
    {
        permissionLevel = givenPermissionLevel;
    }

    public void setStudentName(String givenName)
    {
        studentName = givenName;
    }

    public String getStudentName(){return studentName;}

    public static int generateScriptID()
    {
        //not secure, and may cause collision! change later!
        return ThreadLocalRandom.current().nextInt(1, 100000 + 1);
    }

    //add an answer to this script
    public void addAnswer(Answer toAdd)
    {
        //the list can be null if the object has been created using gson (constructor not called)
        //so we need to check
        if (answers == null)
        {
            answers = new ArrayList<>();
        }
        answers.add(toAdd);
    }

    public void addAnswers(List<Answer> toAdd)
    {
        for (int i = 0; i < toAdd.size(); i++)
        {
            addAnswer(toAdd.get(i));
        }
    }

    //get all the answers of this script
    public List<Answer> getAnswers()
    {
        return answers;
    }

    //get the answer with the given index
    public Answer getAnswer(int index)
    {
        return answers.get(index);
    }

    public int getNoOfAnswers()
    {
        return answers.size();
    }
}
