const fs = require("fs"); //file systems

scripts = []; //list of scripts stored in the database


//sets up the script database
module.exports.initDatabase = function(scriptslocation, dbFileLocation, serverAddress) 
{
	//get a list of all scripts on the server
	var scriptFilenames = fs.readdirSync(scriptslocation);

	//for each script, create a script object and write to database file
	for (var i = 0; i < scriptFilenames.length; i++)
	{
		var scriptObj = createScriptObject(scriptFilenames[i], serverAddress, scriptslocation);

		scripts.push(scriptObj);

		//write the details as JSON to the file
		fs.appendFileSync(dbFileLocation, JSON.stringify(scriptObj) + "\n"); 
	}

	console.log("Database set up!");
}

//load an existing database file
module.exports.loadDatabase = function(dbFileLocation)
{
	var rawJSON = fs.readFileSync(dbFileLocation, {encoding: 'utf-8'});

	//each individual scriptobject is separated by a newline
	var indivJSON = rawJSON.split("\n");
	//the last element will be a new line character alone
	indivJSON.pop();

	//convert each individual json back into an object and place in array
	scripts = [];
	for (var i = 0; i < indivJSON.length; i++)
	{
		scripts.push(JSON.parse(indivJSON[i]));
	}

	console.log("Database loaded!");
}

//returns a list of all the scripts
module.exports.getAllScripts = function()
{
	return JSON.stringify(scripts);
}

module.exports.getAllScriptIDs = function()
{
	var ids = [];
	for (var i = 0; i < scripts.length; i++)
	{
		ids.push(scripts[i].ID);
	}

	return ids;
}

//gets the database entry for the given script. Returns undefined if no such script is present
module.exports.getScriptInfoByID = function(givenId)
{
	var foundScript = scripts.find(
		function(element)
		{
			return (element.ID == givenId);
		});

	return foundScript;
}

//returns true if the given id already exists in the database
function idExists(givenId)
{
	//if searching for the script returns anything but undefined, the script is present
	return (typeof module.exports.getScriptInfoByID(givenId) != 'undefined');
}

function createScriptObject(givenFileLocation, serverAddress, scriptslocation)
{
	var newScript = {
		ID: getNewScriptID(), //the id of the script
		localFileLocation: scriptslocation + givenFileLocation, //the location of the script on the server
		url: serverAddress + "scripts/" + givenFileLocation //the url used to access the script
	}

	return newScript;
}

//returns a script id not in use
function getNewScriptID()
{
	//maximum and mimimum allowable id numbers
	const maxID = 10000; 
	const minID = 0;

	//create new ids randomly until one that is not in use if found
	do
	{
		var newID = Math.floor(Math.random() * (maxID - minID)) + minID;
	}
	while(idExists(newID));


	return newID;

}