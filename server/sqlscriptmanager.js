//a version of "scriptmanager" that use sqlite3 instead of raw JSON
const sessionLength = 3600; //the time in seconds each session is valid for.

const fs = require("fs"); //file systems
const sql = require("sqlite3").verbose(); //sql

const errorHandler = require("./errorhandler.js");
var db;
//sets up the script database
module.exports.loadDatabase = function(scriptsDir, dbFileLocation, serverAddress) 
{
	//open the sql database
	db = new sql.Database(dbFileLocation);
	console.log("Database ready!");
}

module.exports.closeDatabase = function()
{
	db.close();
}

//sends all script ids to the callback
module.exports.sendAllScriptIDs = function(args)
{
	callback = args[0];

	let stmt = "SELECT scripts.id FROM scripts;";
	db.all(stmt, [], (err, rows) => {
	  if (err) {
	    errorHandler.handleScriptIDsError(callback);
	  }
	  //extract the ids themselves and place in an array
	  values = [];
	  rows.forEach((row) =>
	  {
	  	values.push(row.id);
	  });

	  //send the array
	  callback.send(values);
	});
	
}

//sends the db entry for the given script as JSON
module.exports.sendScriptInfo = function(args)
{
	callback = args[0];
	id = args[1];

	let stmt = "SELECT * FROM scripts where scripts.id = ?;";
	db.get(stmt, [id], (err, row) => {
	  if (err) {
	    errorHandler.handleScriptInfoError(response, id);
	  }

	  //send
	  callback.contentType('json');
	  callback.send(JSON.stringify(row));
	});
}

//sends the script info plus the permission level for the given user
module.exports.sendScriptInfoUser = function(args)
{
	callback = args[0];
	givenScriptID = args[1];
	givenUsername = args[2];

	let stmt = "SELECT scripts.id, scripts.studentName, scriptPermissions.permissionLevel FROM scripts INNER JOIN scriptPermissions ON scriptPermissions.scriptID=scripts.id WHERE (scriptPermissions.scriptID=? AND scriptPermissions.username=? AND scriptPermissions.permissionLevel != 0);";

	db.get(stmt, [givenScriptID, givenUsername], (err, row) => {
	  if (err) {
	    errorHandler.handleScriptInfoUserError(callback, givenScriptID, givenUsername);
	  }
	  //if no entry was found, send an empty string
	  if (typeof row == "undefined")
	  {
	  	callback.send(null);
	  }
	  else
	  {
	  	console.log("Sent script info for script ID: " + givenScriptID + ". User: " + givenUsername);
	  	callback.contentType('json');
	  	callback.send(JSON.stringify(row));
	  }
	  
	});
}

//send the ids of all the answers that make up the given script
module.exports.sendAnswerIDs = function(args)
{
	callback = args[0];
	givenScriptID = args[1];

	let stmt = "SELECT answers.id FROM answers WHERE answers.scriptID = ?;";

	db.all(stmt, [givenScriptID], (err, rows) => {
	  if (err) {
	    errorHandler.handleAnswerIDsError(callback);
	  }
	  //extract the ids themselves and place in an array
	  values = [];
	  rows.forEach((row) =>
	  {
	  	values.push(row.id);
	  });

	  console.log("Sent answer ids for script: " + givenScriptID);
	  //send the array
	  callback.send(values);
	});
}


//finds and sends the script with the given id to the callback
module.exports.findAndSendAnswer = function(args)
{
	callback = args[0]; //the callback function is the first argument
	answerId = args[1]; //the answer to send is the second

	//find the answer's location
	let stmt = "SELECT answers.serverFileLocation FROM answers WHERE answers.id = ?;";
	db.get(stmt, [answerId], (err, row) => {
	  if (err) {
	    errorHandler.handleScriptLocNotFound(callback);
	  }
		  //read file and send
		  fs.readFile(row.serverFileLocation, function (error,data){
		  	if (error)
		  	{
		  		errorHandler.handleScriptReadError(callback, answerId, row.serverFileLocation);
		  	}
		  	else
		  	{
		  		callback.contentType("application/pdf");
				callback.send(data);
				console.log("Sent answer: " + answerId);
		  	}
			
		});
	});
}

module.exports.sendAnswerInfo = function(args)
{
	callback = args[0]; //the callback function is the first argument
	answerId = args[1]; //the answer to send is the second

	let stmt = "SELECT * FROM answers where answers.id = ?;";
	db.get(stmt, [answerId], (err, row) => {
	  if (err) {
	    errorHandler.handleScriptInfoError(response, id);
	  }
	  
	  //send
	  callback.contentType('json');
	  callback.send(JSON.stringify(row));

	  console.log("Sent info on answer: " + answerId);

	});
}

//send the ids of all ids associated with the given answer id
module.exports.sendOverlayIDs = function(args)
{
	callback = args[0]; //the callback function is the first argument
	answerId = args[1]; //the answer id is the second

	let stmt = "SELECT overlays.id FROM overlays WHERE overlays.answerId = ?;";

	db.all(stmt, [answerId], (err, rows) => {
	  if (err) {
	    errorHandler.handleOverlayIDError(callback, answerId);
	  }
	  //extract the ids themselves and place in an array
	  values = [];
	  rows.forEach((row) =>
	  {
	  	values.push(row.id);
	  });

	  //send the array
	  callback.send(values);

	  console.log("Sent overlay ids for answer: " + answerId);
	});
}

module.exports.sendOverlayInfo = function(args)
{
	callback = args[0]; //the callback function is the first argument
	overlayId = args[1]; //the answer id is the second

	let stmt = "SELECT * FROM overlays WHERE overlays.id = ?;";

	//since "id" is the primary key, there should be one result
	db.get(stmt, [overlayId], (err, row) => {
	  if (err) {
	    errorHandler.handleOverlayInfoError(callback, overlayId);
	  }
	  
	  //send
	  callback.contentType('json');
	  callback.send(JSON.stringify(row));
	  console.log("Sent info on overlay: " + overlayId);
	  
	});
}

module.exports.sendOverlay = function(args)
{
	callback = args[0]; //the callback function is the first argument
	overlayId = args[1]; //the overlay to send is the second


	//find the overlay's location
	let stmt = "SELECT overlays.serverFileLocation FROM overlays WHERE overlays.id = ?;";
	db.get(stmt, [overlayId], (err, row) => {
	  if (err) {
	    errorHandler.handleOverlayLocNotFound(callback);
	  }
		  //read file and send
		  fs.readFile(row.serverFileLocation, function (error,data){
		  	if (error)
		  	{
		  		errorHandler.handleOverlayReadError(callback, overlayId, row.serverFileLocation);
		  	}
		  	else
		  	{
		  		//unlike answers, overlays are saved as pngs.
		  		callback.contentType("png");
				callback.send(data);
				console.log("Sent overlay: " + overlayId);
		  	}
			
		});
	});


}


module.exports.login = function(response, username, password)
{
	//look up username
	let stmt = "SELECT COUNT(*) as noUsers FROM markers WHERE markers.username=?;";
	db.get(stmt, [username], (err, row) => {
	  if (err) {
	    errorHandler.handleMarkersError(response);
	  }
	  
	  //if count = 1, username exists
	  if (row.noUsers == 1)
	  {
	  	console.log("User: " + username + " found.");
	  	//check password next
	  	checkPassword(response, username, password);
	  }
	  else //unrecognised username
	  {
	  	errorHandler.handleUnknownUsername(response, username);
	  }
	});
	
}

function checkPassword(response, username, password)
{
	let stmt = "SELECT markers.passwordHash FROM markers WHERE markers.username=?;";
	db.get(stmt, [username], (err, row) => {
	  if (err) {
	    errorHandler.handleMarkersError(response);
	  }
	  
	  //if the hashes are equal, return and add a token
	  if (row.passwordHash == password)
	  {
	  	console.log("Correct password!");
	  	response.status(200); //happy camper
	  	//send a token
	  	sendToken(response, username);
	  }
	  else
	  {
	  	errorHandler.handleIncorrectPassword(response);
	  }
	});
}

//sends an access token
function sendToken(response, username)
{
	
	//generate a new token
	var token = getNewToken();

	//get current time
	var curTime = getCurrentUnixTime();

	//set the expiry to be 1 hour from now (60*60 = 3600 seconds)
	var expiryTime = curTime + 3600;
	
	//add to database. if successful, return the token
	const stmt = "INSERT INTO tokens (token, ownerUsername, expiry) VALUES (?, ?, ?);"	


	db.run(stmt, [token, username, expiryTime], function(err) {
		if (err)
		{
			errorHandler.handleAddTokenError(response, sendToken, err, token, username, db);
		}
		else
		{
			console.log("Token issued: " + token + " to: " + username + " Expires: " + unixToReadable(expiryTime));
			response.contentType('json');
			response.send(JSON.stringify(token));
		}
	});
		
		

}

function getCurrentUnixTime()
{
	var date = new Date();

	//getTime returns the milliseconds since 1/1/1970. by dividing by 1000 and rounding, we get the current unix time
	var curTime = date.getTime();
	return Math.round(curTime/1000);
}

//converts unix time to a human readable format (used for debug only)
function unixToReadable(unixtime)
{
	var date = new Date(unixtime * 1000);
	var string = date.getDate().toString() + "/" + (date.getMonth()+1).toString() + "/" + date.getFullYear().toString();
	string += " ";
	string += date.getHours().toString() + ":" + date.getMinutes().toString() + ":" + date.getSeconds().toString();

	return string;
}

function getNewToken()
{
	//maximum and mimimum allowable results
	const maxVal = 10000000; 
	const minVal = 10;

	//use Math.random to generate a random token
	//!! THIS IS VERY INSECURE FOR ACTUAL USE !!
	//use a better source of randomness!

	return (Math.floor(Math.random() * (maxVal - minVal)) + minVal);

}

//checks if the given token is valid and not expired
//functionToRun will be called if the token is valid, passing it the arguments as an array.
module.exports.checkTokenAndRun = function(callback, callbackArgs, token)
{

	//the first argument should be the response
	response = callbackArgs[0];

	const stmt = "SELECT expiry, ownerUsername FROM tokens WHERE token=?;";
	db.get(stmt, [token], (err, row) => {
	  if (err) {
	    return console.error(err.message);
	  }

	  if (typeof row == "undefined") //no token found
	  {
	  	errorHandler.handleUnknownToken(response, token);
	  }
	  else if (row.expiry < getCurrentUnixTime()) //token found; expired
	  {
	  	errorHandler.handleExpiredToken(response, token, db);
	  }
	  else //token valid
	  {
	  	//console.log("Token: " + token + "accepted. Expiry: " + row.expiry);
	  	//run the requested function
	  	callback(callbackArgs);

	  }
	  
	});

}

module.exports.deleteAllTokens = function()
{
	const stmt = "DELETE FROM tokens;";
	db.run(stmt, [], function(err)
	{
		if (err)
		{
			console.log("ERROR");
		}
		else
		{
			console.log("All tokens deleted");
		}
	});
}

//adds a new script to the database
function addScript(givenLocation)
{
	
	const stmt = "INSERT INTO scripts (serverFileLocation) VALUES (?);"	


	db.run(stmt, [givenLocation], function(err) {
		if (err)
		{
			//error code 19 means unique constraint failed i.e. script was already in database
			if (err.errno == 19) 
			{
				console.log("Script already in database. Skipping: " + givenLocation);
			}
			else
			{
				console.err(err.message);
			}
		}
		else
		{
			console.log("New Script Added: " + givenLocation);
		}
		
		
	});


}