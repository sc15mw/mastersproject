//each function handles a different error and sends the appropriate response to the client

//LOGIN ERRORS
//the user entered an unrecognised username
module.exports.handleUnknownUsername = function(response, username)
{
	console.log("User: " + username + " not found.");
	response.status(401); //unauthorised
	response.send("Username not found");
}

//the user entered a valid username, but the password entered did not match
module.exports.handleIncorrectPassword = function(response)
{
	console.log("Invalid login");
  	response.status(401); //unauthorised
  	response.send("Incorrect password");
}


//TOKEN ERRORS
//the randomly generated token is already being used
module.exports.handleAddTokenError = function(response, addFunction, error, token, username, database)
{
	//error code 19 means unique constraint failed
	//either the token generated is already in use, or this user already has a token
	if (error.errno == 19 && error.message.includes("tokens.token"))
	{
		//if the token is already in use
		tokenCollision(response, username);			
	}
	else if (error.errno == 19 && error.message.includes("tokens.ownerUsername"))
	{
		userAlreadyHasToken(response, addFunction, database, username);		
	}
	else
	{
		module.exports.handleUnknownError(response);
	}
	
}

//this is quite rare; the same token has been generated twice independently
//rerunning will generate a new token which should be unique
//there is potential for an infinite loop here. maybe add a limit?
function tokenCollision(response, username)
{
	addFunction(response, username);
}

//the user has already had a token issued
function userAlreadyHasToken(response, addFunction, database, username)
{
	//delete their old token(s) and try adding a new one again
	const delstmt = "DELETE FROM tokens WHERE ownerUsername=?;";
	database.run(delstmt, [username], function(err)
	{
		if (err)
		{
			//if an error occurs here, give up; there is something wrong with the database
			module.exports.handleUnknownError(response);
		}
		else
		{
			console.log("Old token deleted");
			//re-call the add token function. this error will not occur again
			addFunction(response, username);

		}
	});
}

//a token was provided, but it was not found in the database
module.exports.handleUnknownToken = function(response, token)
{
	console.log("Token: " + token + " not found");
	response.status(401); //unauthorised
	response.send("Invalid Token");
}

//the token given was valid, but has expired
module.exports.handleExpiredToken = function(response, token, database)
{
	console.log("Token expired!");
	console.log("Expires: " + unixToReadable(row.expiry));
  	console.log("Current time: " + unixToReadable(getCurrentUnixTime()));

  	response.status(401);
  	response.send("Session expired");


  	
  	//delete the token
  	const stmt = "DELETE FROM tokens WHERE token=?;";
	database.run(stmt, [token], function(err)
	{
		if (err)
		{
			console.log("Error deleting token");
		}
		else
		{
			console.log("Token deleted");
		}
	});
}

module.exports.handleOverlayLocNotFound = function(response)
{

}

module.exports.handleOverlayReadError = function(response, overlayID, overlayLocation)
{
	console.log("Overlay: " + overlayID + " could not be read from: " + overlayLocation);
}

module.exports.handleOverlayIDError = function(response, answerID)
{
	console.log("Could not retrieve a list of overlay ids for answer: " + answerID);
}

module.exports.handleOverlayInfoError = function(response, overlayID)
{
	console.log("Could not retrieve db entry for overlay: " + overlayID);
}
//DATABASE ERRORS
//the database could not retrieve a list of all the script ids
module.exports.handleScriptIDsError = function(response)
{
	console.log("Could not retrieve script ids");
}

module.exports.handleAnswerIDsError = function(response)
{
	console.log("Could not retrieve answer ids");
}

//the database could not retrieve a particular entry from the script database
module.exports.handleScriptInfoError = function(response, scriptID)
{
	console.log("Could not retrieve info for script: " + scriptID);
}

//the database could not find info on a particular script's info concerning a particular use
module.exports.handleScriptInfoUserError = function(response, scriptID, username)
{
	console.log("Could not retrieve info for script: " + scriptID + " relating to user: " + username);
}

//the database could not find the location for a particular script
module.exports.handleScriptLocNotFound = function(response, scriptID)
{
	console.log("Could not find where script: " + scriptID + " is stored on server.");
}

//the script's (supposed) location has been found, but the file could not be read
module.exports.handleScriptReadError = function(response, scriptID, fileLocation)
{
	console.log("Error reading script: " + scriptID + " from: " + fileLocation);
}

//the "markers" table could not be accessed
module.exports.handleMarkersError = function(response)
{

}

//the parts of the script could not be read
module.exports.handlePartError = function(response, scriptID)
{
	console.log("Error finding the parts for script: " + scriptID);
}

//an unknown fatal error (should be called as little as possible)
module.exports.handleUnknownError = function(response)
{
	//tell the client we messed up, so it can shut down gracefully
	response.status(500); //internal server error
	response.send("Unknown error occurred.");
}