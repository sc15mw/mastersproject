//handles logins, session tokens etc.

const sessionLength = 3600; //the time in seconds each session is valid for.

const fs = require("fs"); //file systems
const sql = require("sqlite3").verbose(); //sql

const errorHandler = require("./errorhandler.js");

var db;

module.exports.loadDatabase = function(dbFileLocation) 
{

	//open the sql database
	db = new sql.Database(dbFileLocation);
	console.log("Token database ready!");
}

module.exports.login = function(response, username, password)
{
	//look up username
	let stmt = "SELECT COUNT(*) as noUsers FROM markers WHERE markers.username=?;";
	db.get(stmt, [username], (err, row) => {
	  if (err) {
	    errorHandler.handleMarkersError(response);
	  }
	  
	  //if count = 1, username exists
	  if (row.noUsers == 1)
	  {
	  	console.log("User: " + username + " found.");
	  	//check password next
	  	checkPassword(response, username, password);
	  }
	  else //unrecognised username
	  {
	  	errorHandler.handleUnknownUsername(response, username);
	  }
	});
	
}

function checkPassword(response, username, password)
{
	console.log(username);
	console.log(password);
	let stmt = "SELECT markers.passwordHash FROM markers WHERE markers.username=?;";
	db.get(stmt, [username], (err, row) => {
	  if (err) {
	    errorHandler.handleMarkersError(response);
	  }
	  
	  //if the hashes are equal, return and add a token
	  if (row.passwordHash == password)
	  {
	  	console.log("Correct password!");
	  	response.status(200); //happy camper
	  	//send a token
	  	sendToken(response, username);
	  }
	  else
	  {
	  	errorHandler.handleIncorrectPassword(response);
	  }
	});
}

//sends an access token
function sendToken(response, username)
{
	
	//generate a new token
	var token = getNewToken();

	console.log(token);

	//get current time
	var curTime = getCurrentUnixTime();

	//set the expiry to be 1 hour from now (60*60 = 3600 seconds)
	var expiryTime = curTime + 3600;
	
	//add to database. if successful, return the token
	const stmt = "INSERT INTO tokens (token, ownerUsername, expiry) VALUES (?, ?, ?);"	


	db.run(stmt, [token, username, expiryTime], function(err) {
		if (err)
		{
			errorHandler.handleAddTokenError(response, sendToken, err, token, username, db);
		}
		else
		{
			console.log("Token issued: " + token + " Expires: " + unixToReadable(expiryTime));
			response.send(JSON.stringify(token));
		}
	});
		
		

}

function getCurrentUnixTime()
{
	var date = new Date();

	//getTime returns the milliseconds since 1/1/1970. by dividing by 1000 and rounding, we get the current unix time
	var curTime = date.getTime();
	return Math.round(curTime/1000);
}

//converts unix time to a human readable format (used for debug only)
function unixToReadable(unixtime)
{
	var date = new Date(unixtime * 1000);
	var string = date.getDate().toString() + "/" + (date.getMonth()+1).toString() + "/" + date.getFullYear().toString();
	string += " ";
	string += date.getHours().toString() + ":" + date.getMinutes().toString() + ":" + date.getSeconds().toString();

	return string;
}

function getNewToken()
{
	//maximum and mimimum allowable results
	const maxVal = 10000000; 
	const minVal = 10;

	//use Math.random to generate a random token
	//THIS IS VERY INSECURE FOR ACTUAL USE!!

	return (Math.floor(Math.random() * (maxVal - minVal)) + minVal);

}

//checks if the given token is valid and not expired
//functionToRun will be called if the token is valid, passing it the arguments as an array.
module.exports.checkTokenAndRun = function(callback, callbackArgs, token)
{
	console.log(token);

	const stmt = "SELECT expiry, ownerUsername FROM tokens WHERE token=?;";
	db.get(stmt, [token], (err, row) => {
	  if (err) {
	    return console.error(err.message);
	  }

	  if (typeof row == "undefined") //no token found
	  {
	  	errorHandler.handleUnknownToken(response, token);
	  }
	  else if (row.expiry < getCurrentUnixTime()) //token found; expired
	  {
	  	errorHandler.handleExpiredToken(response, token, db);
	  }
	  else //token valid
	  {
	  	console.log("Expires: " + unixToReadable(row.expiry));
	  	console.log("Current time: " + unixToReadable(getCurrentUnixTime()));
	  	console.log("Token owner: " + row.ownerUsername);
	  	console.log("Token valid!");

	  	//run the requested function
	  	callback(callbackArgs);

	  }
	  
	});

}

module.exports.deleteAllTokens = function()
{
	const stmt = "DELETE FROM tokens;";
	db.run(stmt, [], function(err)
	{
		if (err)
		{
			console.log("ERROR");
		}
		else
		{
			console.log("All tokens deleted");
		}
	});
}