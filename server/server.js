//SERVER SETTINGS
const port = 8002; //port to listen to request from
const serverAddress = "http://localhost:8002/"; //this server's address
const serverDirectory = "/home/matt/uni/postgrad/thesis/prototypes/server/"; //the directory the server is running from
const scriptslocation = serverDirectory + "scripts/"; //the local directory the scripts are stored
const overlaylocation = serverDirectory + "overlays/"; //		"" 			the overlays are stored
const databaselocation = serverDirectory + "scripts.db"; // 	"" 			the script database is stored
//END OF SERVER SETTINGS

const fs = require("fs"); //file systems
const express = require("express"); //express
const bodyParser = require("body-parser");
const router = express.Router(); //express's request router
const multer = require("multer"); //used for handling uploaded files



const scriptmanager = require("./sqlscriptmanager.js");

//on shutdown
process.on('SIGTERM', shutDown);
process.on('SIGINT', shutDown);


const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//load script database
scriptmanager.loadDatabase(scriptslocation, databaselocation, serverAddress);



//top level
//return the ids of all the scripts
app.get('/scriptIDs', function(request, response){
	//get a list of all stored scripts
	//scriptmanager.sendAllScriptIDs(response); 

	var token = request.query.token;
	scriptmanager.checkTokenAndRun(scriptmanager.sendAllScriptIDs, [response], token)
});

//return all the info for the given script
app.get('/getscriptinfo', function(request, response){
	//get the db entry for the given script
	var scriptID = request.query.id;

	var token = request.query.token;
	//scriptmanager.sendScriptInfo(response, scriptID);
	scriptmanager.checkTokenAndRun(scriptmanager.sendScriptInfo, [response, scriptID], token);
});

//return all the info for a given script, as it pertains to the given user (e.g. permissions to view/edit etc.)
app.get('/getscriptinfouser', function(request, response){
	//get the db entry for the given script
	var scriptID = request.query.scriptid;
	var username = request.query.username;

	var token = request.query.token;
	
	//scriptmanager.sendScriptInfoUser(response, scriptID, username);
	scriptmanager.checkTokenAndRun(scriptmanager.sendScriptInfoUser, [response, scriptID, username], token);
});

//return the pdf for the given answer
app.get('/getanswer', function(request, response){
	//get the requested file id from the url
	var fileID = request.query.id;

	var token = request.query.token;

	scriptmanager.checkTokenAndRun(scriptmanager.findAndSendAnswer, [response, fileID], token);
	//scriptmanager.findAndSendAnswer([response, fileID]);

});

//get info on the given answer
app.get('/getanswerinfo', function(request, response){
	//get the requested file id from the url
	var fileID = request.query.id;

	var token = request.query.token;

	//scriptmanager.sendAnswerInfo([response, fileID]);
	scriptmanager.checkTokenAndRun(scriptmanager.sendAnswerInfo, [response, fileID], token);

});

//get the given answer overlay
app.get('/getoverlay', function(request, response){
	//get the requested file id from the url
	var fileID = request.query.id;

	var token = request.query.token;

	scriptmanager.sendOverlay([response, fileID]);

});

//get info on a particular overlay
app.get('/getoverlayinfo', function(request, response){
	//get the requested file id from the url
	var overlayID = request.query.id;

	var token = request.query.token;

	scriptmanager.sendOverlayInfo([response, overlayID]);

	

});

//send a list of ids for the overlays for a specific answer
app.get('/getoverlayids', function(request, response){
	//get the answer id from the id
	var answerID = request.query.id;

	var token = request.query.token;

	
	scriptmanager.sendOverlayIDs([response, answerID]);

});

//return the id of the answers that make up the given script
app.get('/getanswerids', function(request, response){
	//get the script id from the id
	var scriptID = request.query.id;

	var token = request.query.token;

	scriptmanager.checkTokenAndRun(scriptmanager.sendAnswerIDs, [response, scriptID], token);
	//scriptmanager.sendAnswerIDs([response, scriptID]);

});

//login request
app.post('/login', function(request, response){
	//get username and password hash
	var username = request.body.username;
	var password = request.body.password;

	scriptmanager.login(response, username, password);
});

var uploadImage = multer({
	dest: overlaylocation,
})

app.post('/addoverlay', function(request, response){
	console.log("HI!");
	var overlayID = request.body.id;
	console.log(overlayID);
	var answerID = request.body.answerID;
	console.log(answerID);
	response.status(200);
	//response.set("Connection", "close");
	console.log(response);
});

module.exports = router;

app.listen(port, function(){
	console.log(`Now listening on ${port}`)
});

//runs on server shutdown
function shutDown()
{
	//revoke all tokens
	scriptmanager.deleteAllTokens();
	scriptmanager.closeDatabase();
	console.log("Shutting down...");
	process.exit(0);
}